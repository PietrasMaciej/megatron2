OrderViewModel = function()
{
	var self = this;
	self.methodOfPayment = ko.observable();
	self.costOfRepair = ko.observable();
	self.descriptionOfIssue = ko.observable();
	
	self.save= function()
	{
		var data = ko.toJSON(self);
        $.ajax({
            url: "http://localhost:8080/TestProject/resources/orders/add",
            type: "POST",
            data: data,
            contentType: "application/json",
            success: function (data) {
                window.location="addorder.html";
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("blad");
            }
        });
	}
}