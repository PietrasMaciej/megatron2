UserViewModel = function()
{
	var self = this;
	self.username = ko.observable();
	self.email = ko.observable();
	self.password = ko.observable();
	
	self.save= function()
	{
		var data = ko.toJSON(self);
        $.ajax({
            url: "http://localhost:8080/megatron/resources/users/add",
            type: "POST",
            data: data,
            contentType: "application/json",
            success: function (data) {
                window.location="addUser.html";
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("blad");
            }
        });
	}
}