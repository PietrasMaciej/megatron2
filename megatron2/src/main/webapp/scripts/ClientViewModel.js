ClientViewModel = function()
{
	var self = this;
	self.name = ko.observable();
	self.surname = ko.observable();
	self.phoneNumber = ko.observable();
	
	self.save= function()
	{
		var data = ko.toJSON(self);
        $.ajax({
            url: "http://localhost:8080/megatron/resources/clients/add",
            type: "POST",
            data: data,
            contentType: "application/json",
            success: function (data) {
                window.location="addClient.html";
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
               alert("bład");
            }
        });
	}
}