package services.dto;

import java.sql.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatusHistorySummaryDto 
{
	private Date receiveDate;
	private Date inProgress;
	private Date releaseDate;

	public Date getReceiveDate() 
	{
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) 
	{
		this.receiveDate = receiveDate;
	}

	public Date getInProgress() 
	{
		return inProgress;
	}

	public void setInProgress(Date inProgress) 
	{
		this.inProgress = inProgress;
	}

	public Date getReleaseDate() 
	{
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) 
	{
		this.releaseDate = releaseDate;
	}

}
