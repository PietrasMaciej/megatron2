package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

import domain.Ordr.MethodOfPayment;

@XmlRootElement
public class OrdrSummaryDto 
{
	private MethodOfPayment methodOfPayment;
	private float costOfRepair;
	private String descriptionOfIssue;

	public MethodOfPayment getMethodOfPayment() 
	{
		return methodOfPayment;
	}

	public void setMethodOfPayment(MethodOfPayment methodOfPayment) 
	{
		this.methodOfPayment = methodOfPayment;
	}

	public float getCostOfRepair() 
	{
		return costOfRepair;
	}

	public void setCostOfRepair(float costOfRepair) 
	{
		this.costOfRepair = costOfRepair;
	}

	public String getDescriptionOfIssue() 
	{
		return descriptionOfIssue;
	}

	public void setDescriptionOfIssue(String descriptionOfIssue) 
	{
		this.descriptionOfIssue = descriptionOfIssue;
	}
}
