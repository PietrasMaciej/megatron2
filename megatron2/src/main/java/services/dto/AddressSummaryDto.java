package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressSummaryDto 
{
	private String country;
	private String city;
	private String street;
	private String postalCode;
	private String houseNumber;
	
	public String getCountry() 
	{
		return country;
	}
	public void setCountry(String country) 
	{
		this.country = country;
	}
	public String getCity() 
	{
		return city;
	}
	public void setCity(String city) 
	{
		this.city = city;
	}
	public String getStreet() 
	{
		return street;
	}
	public void setStreet(String street) 
	{
		this.street = street;
	}
	public String getPostalCode() 
	{
		return postalCode;
	}
	public void setPostalCode(String postalCode) 
	{
		this.postalCode = postalCode;
	}
	public String getHouseNumber() 
	{
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) 
	{
		this.houseNumber = houseNumber;
	}
}
