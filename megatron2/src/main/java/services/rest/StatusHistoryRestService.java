package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.StatusHistory;
import repositories.IRepositoryCatalog;
import services.dto.StatusHistorySummaryDto;

@Path("statusHistories")
public class StatusHistoryRestService 
{
	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveStatusHistory(StatusHistorySummaryDto statusHistory)
	{
		StatusHistory s = new StatusHistory();
		s.setReceiveDate(statusHistory.getReceiveDate());
		s.setInProgress(statusHistory.getInProgress());
		s.setReleaseDate(statusHistory.getReleaseDate());
		catalog.getStatusHistories().add(s);
		return "OK";
	}
}
