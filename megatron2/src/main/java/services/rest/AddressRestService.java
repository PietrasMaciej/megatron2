package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.Address;
import repositories.IRepositoryCatalog;
import services.dto.AddressSummaryDto;

@Path("addresses")
public class AddressRestService 
{

	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveAddress(AddressSummaryDto address){
		Address a = new Address();
		
		a.setCountry(address.getCountry());
		a.setCity(address.getCity());
		a.setStreet(address.getStreet());
		a.setPostalCode(address.getPostalCode());
		a.setHouseNumber(address.getHouseNumber());
		
		catalog.getAddresses().add(a);
		
		return "OK";
	}
}
