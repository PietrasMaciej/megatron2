package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.Client;
import repositories.IRepositoryCatalog;
import services.dto.ClientSummaryDto;

@Path("clients")
public class ClientRestService
{

	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveClient(ClientSummaryDto client)
	{
		Client c = new Client();
		c.setName(client.getName());
		c.setSurname(client.getSurname());
		c.setPhoneNumber(client.getPhoneNumber());
		catalog.getClients().add(c);
		return "OK";
	}
}
