package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import domain.User;
import repositories.IRepositoryCatalog;
import services.dto.UserSummaryDto;

@Path("users")
public class UserRestService 
{

	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveUser(UserSummaryDto user)
	{
		User u = new User();
		u.setUsername(user.getUsername());
		u.setEmail(user.getEmail());
		u.setPassword(user.getPassword());
		catalog.getUsers().add(u);
		return "OK";
	}
	
	@GET
	@Produces("text/html")
	public String test()
	{
		return "testowy response z rest serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<UserSummaryDto> getUsers()
	{
		List<UserSummaryDto> result = new ArrayList<UserSummaryDto>();
		
		List<User> allUsers = catalog.getUsers().getAll();
		
		for(User u : allUsers)
		{
			UserSummaryDto user = new UserSummaryDto();
			user.setUsername(u.getUsername());
			user.setEmail(u.getEmail());
			user.setPassword(u.getPassword());
			result.add(user);
		}
		return result;
	}
}
