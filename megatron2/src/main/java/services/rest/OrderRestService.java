package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.Ordr;
import repositories.IRepositoryCatalog;
import services.dto.OrdrSummaryDto;

@Path("orders")
public class OrderRestService
{
	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveOrder(OrdrSummaryDto order)
	{
		Ordr o = new Ordr();
		o.setMethodOfPayment(order.getMethodOfPayment());
		o.setCostOfRepair(order.getCostOfRepair());
		o.setDescriptionOfIssue(order.getDescriptionOfIssue());
		catalog.getOrders().add(o);
		return "OK";
	}
}
