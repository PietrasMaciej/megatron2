package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.Employee;
import repositories.IRepositoryCatalog;
import services.dto.EmployeeSummaryDto;

@Path("employees")
public class EmployeeRestService 
{
	@Inject
	IRepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveEmployee(EmployeeSummaryDto employee)
	{
		Employee e = new Employee();
		e.setName(employee.getName());
		e.setSurname(employee.getSurname());
		e.setPhoneNumber(employee.getPhoneNumber());
		catalog.getEmployees().add(e);
		return "OK";
	}
}
