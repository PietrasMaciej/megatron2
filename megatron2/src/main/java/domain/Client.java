package domain;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name="client.all", query="SELECT c FROM Client c")
public class Client extends EntityBase
{
	
	public Client()
	{
		this.addresses=new HashSet<Address>();
	}
	
	private String name;
	
	private String surname;
	
	private String phoneNumber;
	
	@OneToOne
	private User user;
	
	//@OneToMany(mappedBy="Client", cascade=CascadeType.PERSIST)
	private Set<Address> addresses;
	
	//@OneToMany(mappedBy="Client", cascade=CascadeType.PERSIST)
	//private Set<Ordr> orders;
	
	

	public String getName() 
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public User getUser() 
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	/*public Set<Ordr> getOrders() {
		return orders;
	}

	public void setOrders(Set<Ordr> orders) {
		this.orders = orders;
	}
	*/
	
}
