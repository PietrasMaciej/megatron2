package domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQuery(name="ordr.all", query="SELECT o FROM Ordr o")
public class Ordr extends EntityBase {
	public enum MethodOfPayment
{
	cash, creditCard
}
	
	private MethodOfPayment methodOfPayment;
	private float costOfRepair;
	private String descriptionOfIssue;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Client client;
	
	@ManyToMany
	private Set<Employee>employee;
	
	//@OneToMany(mappedBy="Ordr", cascade=CascadeType.PERSIST)
	//private Set<StatusHistory> statusHistory;
	
	

	public MethodOfPayment getMethodOfPayment() 
	{
		return methodOfPayment;
	}

	public void setMethodOfPayment(MethodOfPayment methodOfPayment) 
	{
		this.methodOfPayment = methodOfPayment;
	}

	public float getCostOfRepair() 
	{
		return costOfRepair;
	}

	public void setCostOfRepair(float costOfRepair) 
	{
		this.costOfRepair = costOfRepair;
	}

	public String getDescriptionOfIssue() 
	{
		return descriptionOfIssue;
	}

	public void setDescriptionOfIssue(String descriptionOfIssue) 
	{
		this.descriptionOfIssue = descriptionOfIssue;
	}

	public Client getClient() 
	{
		return client;
	}

	public void setClient(Client client) 
	{
		this.client = client;
	}

	public Set<Employee> getEmployee() 
	{
		return employee;
	}

	public void setEmployee(Set<Employee> employee) 
	{
		this.employee = employee;
	}

	/*public Set<StatusHistory> getStatusHistory() 
	{
		return statusHistory;
	}

	public void setStatusHistory(Set<StatusHistory> statusHistory)
	{
		this.statusHistory = statusHistory;
	}*/

}
