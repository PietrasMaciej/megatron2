package domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name="employee.all", query="SELECT e FROM Employee e")
public class Employee extends EntityBase
{

	private String name;
	private String surname;
	private String phoneNumber;
	
	@OneToOne
	private Address address;
	
	//@ManyToMany(mappedBy="Employee", cascade=CascadeType.PERSIST)
	//private Set<Ordr> orders;
	
	
	

	/*public Set<Ordr> getOrders() 
	{
		return orders;
	}

	public void setOrders(Set<Ordr> orders) 
	{
		this.orders = orders;
	}
*/
	public String getName()
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getSurname() 
	{
		return surname;
	}

	public void setSurname(String surname) 
	{
		this.surname = surname;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() 
	{
		return address;
	}

	public void setAddress(Address address) 
	{
		this.address = address;
	}

}
