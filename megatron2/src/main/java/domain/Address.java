package domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;


@Entity
@NamedQuery(name="address.all", query="Select a from Address a")

public class Address extends EntityBase 
{

	private String country;
	private String city;
	private String street;
	private String postalCode;
	private String houseNumber;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Client client;
	
	@OneToOne
	private Employee employee;
	
	
	public String getCountry() 
	{
		return country;
	}
	public void setCountry(String country) 
	{
		this.country = country;
	}
	public String getCity() 
	{
		return city;
	}
	public void setCity(String city) 
	{
		this.city = city;
	}
	public String getStreet() 
	{
		return street;
	}
	public void setStreet(String street) 
	{
		this.street = street;
	}
	public String getPostalCode() 
	{
		return postalCode;
	}
	public void setPostalCode(String postalCode) 
	{
		this.postalCode = postalCode;
	}
	public String getHouseNumber() 
	{
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) 
	{
		this.houseNumber = houseNumber;
	}
	public Client getClient() 
	{
		return client;
	}
	public void setClient(Client client) 
	{
		this.client = client;
	}
	public Employee getEmployee() 
	{
		return employee;
	}
	public void setEmployee(Employee employee) 
	{
		this.employee = employee;
	}
	
	
}
