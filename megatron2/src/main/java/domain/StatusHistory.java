package domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


@Entity
@NamedQuery(name="statusHistory.all", query="SELECT s FROM StatusHistory s")
public class StatusHistory extends EntityBase
{
	private Date receiveDate;
	private Date inProgress;
	private Date releaseDate;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Ordr order;
	

	public Date getReceiveDate() 
	{
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) 
	{
		this.receiveDate = receiveDate;
	}

	public Date getInProgress() 
	{
		return inProgress;
	}

	public void setInProgress(Date inProgress) 
	{
		this.inProgress = inProgress;
	}

	public Date getReleaseDate() 
	{
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) 
	{
		this.releaseDate = releaseDate;
	}

	public Ordr getOrder() 
	{
		return order;
	}

	public void setOrder(Ordr order) 
	{
		this.order = order;
	}

	
}
