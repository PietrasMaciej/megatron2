package repositories;

import domain.*;

public interface IRepositoryCatalog 
{
	public IRepository<Address> getAddresses();
	public IRepository<Client> getClients();
	public IRepository<Employee> getEmployees();
	public IRepository<Ordr> getOrders();
	public IRepository<StatusHistory> getStatusHistories();
	public IRepository<User> getUsers();
}
