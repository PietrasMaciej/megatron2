package repositories;

import java.util.List;

import domain.EntityBase;

public interface IRepository <TEntity extends EntityBase>
{
	
	public void delete(TEntity entity);
	public void update(TEntity entity);
	public void add(TEntity entity);
	
	public TEntity get(int id);
	public List<TEntity> getAll();
	
}
