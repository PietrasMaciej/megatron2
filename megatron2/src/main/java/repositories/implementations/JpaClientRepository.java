package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Client;
import repositories.IRepository;

public class JpaClientRepository implements IRepository<Client> 
{

	private EntityManager em;
	
	public JpaClientRepository(EntityManager em) 
	{
		this.em = em;
	}

	@Override
	public Client get(int id) 
	{
		return em.find(Client.class, id);
	}
	
	@Override
	public List<Client> getAll() 
	{
		return em.createNamedQuery("client.all", Client.class).getResultList();
	}

	@Override
	public void add(Client entity) 
	{
		em.persist(entity);
	}

	@Override
	public void delete(Client entity) 
	{
		em.remove(entity);
	}

	@Override
	public void update(Client entity) 
	{
		
	}
}
