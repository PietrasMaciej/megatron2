package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import domain.StatusHistory;
import repositories.IRepository;

public class JpaStatusHistoryRepository implements IRepository<StatusHistory> 
{
	
	private EntityManager em;
	
	public JpaStatusHistoryRepository(EntityManager em) 
	{
		super();
		this.em = em;
	}

	@Override
	public StatusHistory get(int id) 
	{
		return em.find(StatusHistory.class, id);
	}

	@Override
	public List<StatusHistory> getAll() 
	{
		return em.createNamedQuery("statusHistory.all", StatusHistory.class).getResultList();
	}

	@Override
	public void add(StatusHistory entity)
	{
		em.persist(entity);
	}

	@Override
	public void delete(StatusHistory entity) 
	{
		em.remove(entity);
	}

	@Override
	public void update(StatusHistory entity) 
	{
		
	}
}
