package repositories.implementations;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import domain.*;
import repositories.IRepository;
import repositories.IRepositoryCatalog;

@Stateless
public class RepositoryCatalog implements IRepositoryCatalog 
{
	
	@Inject
    private EntityManager em;

	@Override
	public IRepository<Address> getAddresses() 
	{
		return new JpaAddressRepository(em);
	}
	
	@Override
	public IRepository<Client> getClients() 
	{
		return new JpaClientRepository(em);
	}

	@Override
	public IRepository<Employee> getEmployees() 
	{
		return new JpaEmployeeRepository(em);
	}

	@Override
	public IRepository<Ordr> getOrders() 
	{
		return new JpaOrdrRepository(em);
	}

	@Override
	public IRepository<StatusHistory> getStatusHistories() 
	{
		return new JpaStatusHistoryRepository(em);
	}

	@Override
	public IRepository<User> getUsers() 
	{
		return new JpaUserRepository(em);
	}

}
