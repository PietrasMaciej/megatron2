package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Employee;
import repositories.IRepository;

public class JpaEmployeeRepository implements IRepository<Employee> 
{
	
	private EntityManager em;
	
	public JpaEmployeeRepository(EntityManager em) 
	{
		super();
		this.em = em;
	}

	@Override
	public Employee get(int id) 
	{
		return em.find(Employee.class, id);
	}

	@Override
	public List<Employee> getAll() 
	{
		return em.createNamedQuery("employee.all", Employee.class).getResultList();
	}

	@Override
	public void add(Employee entity)
	{
		em.persist(entity);
	}

	@Override
	public void delete(Employee entity) 
	{
		em.remove(entity);
	}

	@Override
	public void update(Employee entity) 
	{
		
	}
}
