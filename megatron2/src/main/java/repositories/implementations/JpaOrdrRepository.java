package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Ordr;
import repositories.IRepository;

public class JpaOrdrRepository implements IRepository<Ordr> 
{
	
	private EntityManager em;
	
	public JpaOrdrRepository(EntityManager em) 
	{
		super();
		this.em = em;
	}

	@Override
	public Ordr get(int id) 
	{
		return em.find(Ordr.class, id);
	}

	@Override
	public List<Ordr> getAll() 
	{
		return em.createNamedQuery("ordr.all", Ordr.class).getResultList();
	}

	@Override
	public void add(Ordr entity)
	{
		em.persist(entity);
	}

	@Override
	public void delete(Ordr entity) 
	{
		em.remove(entity);
	}

	@Override
	public void update(Ordr entity) 
	{
		
	}
}
