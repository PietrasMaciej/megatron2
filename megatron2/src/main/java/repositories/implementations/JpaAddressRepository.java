package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Address;
import repositories.IRepository;

public class JpaAddressRepository implements IRepository<Address> 
{

	private EntityManager em;
	
	public JpaAddressRepository(EntityManager em) 
	{
		this.em = em;
	}

	@Override
	public Address get(int id) 
	{
		return em.find(Address.class, id);
	}
	
	@Override
	public List<Address> getAll() 
	{
		return em.createNamedQuery("address.all", Address.class).getResultList();
	}

	@Override
	public void add(Address entity) 
	{
		em.persist(entity);
	}

	@Override
	public void delete(Address entity) 
	{
		em.remove(entity);
	}

	@Override
	public void update(Address entity) 
	{
		
	}

}
